#!/usr/bin/env bash

egrep "^  \(symbol" custom.kicad_sym  | sed -r 's/.*\"(.*)\".*/\1\n/g' | egrep -vi "harbor|rackside" > SYMBOLS.md

egrep "^DEF" custom.lib | sed -r 's/#//g'  | sed -r 's/^DEF ([A-Za-z0-9.+_,\-]{1,}) .*/\1\n/g' | egrep -vi "harbor|rackside" > FOOTPRINTS.md
